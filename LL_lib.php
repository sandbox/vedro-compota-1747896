<?php


// @$plid = is parent $node->book['mlid']
function make_node($title,$body,$pid,$plid) // СОЗДАЁМ НОДУ
{
  log_light("0000 node creation>>".$title." is started");
  
  if ($body) $body_text =$body;
  else $body_text = $title;
  
  $node = new stdClass();
  $node->type = 'article';
  node_object_prepare($node);
  
  $node->title    = $title ;//. date('c');
  $node->language = LANGUAGE_NONE;
 
  $node->body[$node->language][0]['value']   = $body_text;
  $node->body[$node->language][0]['summary'] = text_summary($body_text); // аннотация
  $node->body[$node->language][0]['format']  = 'filtered_html'; // формат
  
  
  
  $path = 'content/'.$title;//programmatically_created_node_' . date('YmdHis');
  $node->path = array('alias' => $path);
  
  // ADDITONAL SECTION
  if (isset($pid)) 
  {
    $node = add_back_link($node,$pid); //add back links
   $node = LLBookInfo::TryOutline($node,$plid); //add book info
  }
  
 $node = node_submit($node);
 //print_r($node);
 log_light("$node->book['mlid']");
  if ($node = node_submit($node)) node_save($node);
  //log_light("0000 node creation for >>".$title."  IS COMPLETED");

  return  $node;
  
}

function  add_book_info($node,$pid,$bid) // передаём ноду и ещё что-то на ваше усмотрение
{
   $bid=727; //  nid  ноды-книги 
   
 
 $node->book['plid'] =644;
 $node->book['bid'] =$bid;

 return $node;
   
}

function add_back_link($node,$pid)
{
  log_light("add_back_link section = pid = ".$pid);
  $node->field_88['und']['0']['nid'] = $pid;// THIS NEED STANDERTIZATION
  return $node;
}


/* function strs_btw_tags($opentag,$closetag,$str) // вытащит значения из тэгов вида (тэг1строкатэг2)  - причём первый  - открывающий тэг может быть расширен спец параметрами.
{
   
   log_light("opentag = ",$opentag);
   //$oppart = get_expand_tag_value($opentag);
   $oppart ="(".$opentag.")";
   $clpart = "(".$closetag.")";
      log_light("SSSTTTRRRRRR = ".$str); 
   //$oppart = "(\[".$tagname."\]|\[".$tagname."=\".{0,}\"\])"; // регулярное выражение для открывающего тэга
   // $clpart = "(\[\\\\".$tagname."\])"; //регулярное выражение для закрывающего тэга.
   log_light("--- str_btw_tags()-- we start searching for ".$oppart." and ".$clpart." in << ".$str.">>");
   
   $pattern = "(".$oppart.".*".$clpart.")is"; // шаблон-результат - процесс поиска не зависит от регистра и производится по всему тексту.
	$matches=array();
	if ( $n = preg_match_all($pattern,$str,$matches,PREG_PATTERN_ORDER )!= 0 ) // если не было ошибки или если встретили хотя бы одно подходящее знаение.
	 { 
    	 log_light("--- str_btw_tags()-- WE HAVE matches !  = ".$n);
		
         //log_light("ARR1111111=".arr_to_str($matches[0])); 
		$arr =  free_from_tags($oppart,$clpart,$matches[0]);
	        // log_light("ARR222222=".arr_to_str($arr)); 
		 $result= $arr;
	 }
	 else 
	 {
	 log_light("--- str_btw_tags()-- NO matches ");
	 $result= 0; // просто возвращаем исходный текст
      }
      
	  
	  return $result;y
      
}  */

/*
@$format= 0 or 1 / $matches[0] or $matches[1]
*/
function strs_btw_tags($opentag,$str,$format)
{
   
  $result="";
  
  //$rreg ="|<[^>]+>(.*)</[^>]+>|U"; - это базовый паттерн о нём я пишу на этом же сайте.
  $p1 ="|<";
  $p2 ="[^>]*>(.*)</";
  $p3 =">|Us";
  $tag = free_tag_from_brackets($opentag);
  
  $pattern = $p1.$tag.$p2.$tag.$p3 ; // create pattern
   //$pattern = "|<[^>]+>(.*)</[^>]+>|Us" ; // create pattern
// $pattern = "|<ll[^>]*>(.*)</ll>|Us" ;
 
  //log_light("TEXT = ".$str);
  //log_light("TTag=".$opentag);
 // log_light("pattern=".$pattern);
  
  if ( $n = preg_match_all($pattern,$str,$matches,PREG_PATTERN_ORDER )!= 0 )
  {
    if ($format) $result = $matches[1]; else  $result = $matches[0];
  }
 // log_light("result array 0 =". arr_to_str($matches[0]));
 // log_light("result array 1 =". arr_to_str($matches[1]));
  
  return $result; 
} 

/*Функция получает регулярные выражения для  открывающего и закрывающего тэга+ массив строк 
- и удаляет тяги из каждого элемента массива*/
function free_from_tags($oppart,$clpart,$arr)
{
  $ert = $oppart;//."is";
  $i=0;
  log_light("oppart = ".$ert);
  log_light("close part = ".$clpart);
  
  foreach ($arr as $el)
  {
     	// log_light("1) ELEM =".$el);
	 $el = preg_replace($clpart,"",$el); // сначала убраем закрывающий тэг - так как он проще как выражение
	 	// log_light("2) ELEM =".$el);
		 
	 //if (preg_match_all($ert,$el,$matches,PREG_PATTERN_ORDER )) log_light("88888=".$matches[0][0]); 
	 //else log_light("no matches!");
	 
	 $el = preg_replace($ert,"",$el);
	// log_light("3) ELEM =".$el);
	 $arr[$i]=$el;
	// log_light("free_from_tags [".$i."] = ".$el);
	 $i++;
  }
  
  log_light("ARR = ".arr_to_str($arr));
  return $arr;
}
	 

// - функция вернёт расширенную форму тэга в виде регулярки - разрешив перед последним стандартным символом тэга вставлять какие угодно значения
function get_expand_tag_value($tagstr) 
{
	 $reg="("; // сюда мы созраним регулярное выражение, соответствующее расширенной форме тэга- в начале только ограничитель выражения
	 $n = strlen($tagstr);// ??? функция узнающая длину строки ??????????????????/
	 $arr = str_to_arr($tagstr) ;/* затем преобразуем строчку в массив - копируем группы символов до последнего и последний - 
	  после чего ,склеиваем регулярку, вставив между двумя кусками тэга выражения любых символов добавленных сколько угодно раз.*/

	$m=$n-1;
	log_light("TAG = ".$tagstr." = ".$n." = ".$m);
	for ($i=0;$i<$m;$i++)
    {
	  $reg.=  $arr[$i];
	  log_light($arr[$i]." = ".$i);
	}
	
	$reg.= ".{0,}" ;// добавляем фрагмент  -"какие угодно символы сколько угодно раз"
	//$reg.= ".{0,}" ;// добавляем фрагмент  -"какие угодно символы сколько угодно раз"
	$reg.= $arr[$m]; // добавляем последний символ исходного тэга
	$reg.= ")";
	
	$result = $reg; 
	return  $result; 
    	
}

function  str_to_arr($str)
{
  $arr= preg_split('||', $str, -1, PREG_SPLIT_NO_EMPTY);
  return $arr; 
  
}

 function log_light($str) // лог-функция
 {
   $result = "---------yes!!!)))";
	$filename = LOG_PATH; // "C:\\data\\localhost\\www\\12\\sites\\all\\modules\\my\\light_book\\log.txt"; // имя текстового файла откуда будем брать команды
	$mode = "a+"; // задаём режим чтения файла.
	//$mode = "w";
	if ($fdata = fopen ($filename, $mode))
	{ 
		fwrite($fdata, "[".date("l dS of F Y h:I:s A")."] ".$str." \n" );
		//fwrite($fdata, $mytext);
		//fwrite($fdata, "\n--------------------\n");
		fclose($fdata);
    }
	else 
	{$result = "---------no ************************************************= ".__DIR__."+".LOG_PATH;
		echo "<br>".$result."<br>";
	   print_r(error_get_last());
	}
	//fclose($fdata);
	return $result;
}

function clean_log()
{
  $filename = LOG_PATH; //  например так = "C:\\12_book\\log.txt"; // имя текстового файла откуда будем брать команды
	$mode = "w"; // задаём режим чтения файла.
	if ($fdata = fopen ($filename, $mode)) fclose($fdata); 
}
 
 function getstr2() // test func
 {
    $str = "123412341234124";
    return  $str;
 }
 
 function  aprint($array)
 {
   echo("<pre>");
   print_r($array);
   echo("</pre>");
 }
 
 // функция переводит одномерный массив в строку
 function arr_to_str($arr)
 {
   $str="{ ";
   foreach ($arr as $value)
   {
     $str .=  $value." , ";   
   }
   
   $str.=" }";
   return $str;
 }
 
 function lists_from_strs ($strarr)
 {
    $listarr="";
    foreach ($strarr as $value )
    {
	  $listarr[]=   list_from_str($value); 
	}
	
	return $listarr;
 
 }
 // получить из строки список строк (подстрок - по знаку переноса строки) пхп php
 function list_from_str($str)
 {
    $arr[]=0;
	$subarr = str_to_arr($str);
	$s=""; // переменная для накопления значения строки до очередного знака переноса
	$i=0;// счётчик найденных переносов строки
	foreach ($subarr as $value)
	{
	    if ($value=="\n")//((($value=="\n")||($value=="\r\n"))&&($s!=""))
		{
		   $arr[$i]=$s;
		   $s="";
		   $i++;
		} else if ($value!="\r") $s.=$value; // иначе просто накапливаем строку
	}
	if ($s!="")
		{
		  $arr[$i]=$s;
		  } // заполняем последнее значение - на случай если после него не будет знака переноса строки.
		
	return $arr;
 }
 
function make_drupal_link($txt,$id)
{
    $base=$GLOBALS['base_url']."/"."node/";
	return "<a href=\"".$base.$id."\">".$txt."</a>";
}

 /*удалит из переданного азового тэга угловые скобки и слэш (результат в
 возвращаемом значении)*/
function free_tag_from_brackets($tag)
{
    $result="";   
   $arr= str_to_arr($tag);
	foreach ($arr as $val)
	{
	  if (($val!='<')&&($val!='>')&&($val!='/'))
	  {
	    $result.=$val;   
	  }
	}
	return $result; 
}
