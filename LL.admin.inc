<?php



function LL_admin_settings() { 
  // Get an array of node types with internal names as keys and 
  // "friendly names" as values. E.g., 
  // array('page' => �Basic Page, 'article' => 'Articles') 
 
  $types = node_type_get_types(); 
  foreach($types as $node_type) {  
     $options[$node_type->type] = $node_type->name; 
  }   
   $form['LL_open_tag'] = array(  
    '#type' => 'textfield',   
    '#title' => t('Open LL tag'),  
    '#default_value' => variable_get('LL_open_tag', "<ll>"),  
    '#description' => t('Tag closing the list'),  
  ); 
 
    $form['LL_close_tag'] = array(  
    '#type' => 'textfield',   
    '#title' => t('Close LL tag'),   
    '#default_value' => variable_get('LL_close_tag', '</ll>'),  
    '#description' => t('Tag opennig the list'),  
  ); 
  $form['LL_level_space_dif'] = array(  
    '#type' => 'textfield',   
    '#title' => t('Level difference (in spaces)'),  
    '#default_value' => variable_get('LL_level_space_dif',1),  
    '#description' => t('Need to specify the level of list items by find difference count of spaces at the beginnings of current and previous lines '),  
  ); 
 
 
  $form['#submit'][] = 'LL_admin_settings_submit';
  
  
  return system_settings_form($form);  // ���������� �������� � ������, ������� �� ������ ������ �� ��������� �������� ��� ������� ������ 
}

function LL_admin_settings_validate($form, &$form_state) 
{ // ��� ������� �������� �������� � ���, ��� �� ����� ������ �����, � �� ���-�� ������.
// ��� ������� ����� �������� �������� �������������.
  
  $limit = $form_state['values']['LL_level_space_dif']; 
 if (!is_numeric($limit)) 
   form_set_error('LL_level_space_dif', t('Please enter number.')); 
  
} 

function LL_admin_settings_submit($form, $form_state)
{

 //--------tags-------------
  variable_set("LL_open_tag",$form_state['values']['LL_open_tag']);
  variable_set("LL_close_tag",$form_state['values']['LL_close_tag']);
  
  //----------other------------------------
  variable_set("LL_level_space_dif",$form_state['values']['LL_level_space_dif']);
}

