<?php 

include 'LL_def.php';
include 'LL_lib.php';

/* function massive_gen($strarr)
{
   foreach($strarr as $key => $value) 
   {
      
   }
} */

// Class
/*класс описывает авто-спискок LL*/
class  Llist 
{
  public $titles = "this's LL node"; // массив пунктов списка - заголовков создаваемых страниц
  public $bltext = "[go back]"; // текст для ссылки "домой" back link text
  public $pid ; // системный идентификационный  номер "родителя"  -необходим для формирования обратной ссылки
  public $ids ; //  массив идентификационных номеров страниц, перечисленных в списке
  public $body; // строка содержащая html  представляющий список на странице
  public $plid; //mlid (id) of parent node
  
   function __construct($titles,$pid,$bltext,$plid)
   {
	   $this->titles = $titles;
	   if ($bltext) $this->bltext = $bltext;
	   $this->pid = $pid;
	   $this->plid = $plid;
	   log_light("PPPPPPPiddddd = ".$pid);
   }
   public function IsLink($str) // THIS NEEDS STANDARTIZATION
   {
     $result=0;
	 if (strs_btw_tags("<a>",$str,0)) $result=1;
	 return $result; 
   }
	
   public function Create() //  создаём страницы с соответствующими заголовками.
   {
     log_light("CREATION NODE START creation>>");    
	 $arr=$this->titles;
	 $i=0;
	 $ids=array();// для массива идентификационых номеров
	  foreach ($arr as $title)
			   {
				  if (!$this->IsLink($title)) // checking varian when title already mark as link
				  {
					  log_light("CREATION ".$i." creation>>".$title);
					  $bodytxt="\n-------------\n <B>".make_drupal_link($this->bltext,$this->pid)."</b>"; // THIS NEED STANDERTIZATION
					  $mynode = make_node($title,$bodytxt,$this->pid,$this->plid);
					  $ids[$i]= $mynode->nid; // заполняем массив индексами
				  }
				  $i++;
			   }
			   
			   $this->ids =$ids;
			   
	 log_light("CREATION NODE iS ENDED");
   }
   
   public function PrintLinks() // return html links array (for printing - in string)
   {
      $str ="";// строка -результат
	  $ttls = $this->titles;
      $ids  = $this->ids;
	  log_light("TXT FRAGS = ".arr_to_str($ttls));
	  log_light("IDS = ".arr_to_str($ids));
	  
	  $i=0;
	  $c=count($ttls);
      foreach ($ttls as $value)
      {
	   if (!$this->IsLink($value)) $str.= make_drupal_link($value ,$ids[$i]); else $str.=$value;
	   if (($i+1)<$c) $str.="\n"; 
	   //$str.= "<a href=\"http://ku/node/199\">".$value."</a>";
	   $i++;
	  }	  
    return $this->body=$str; 
    //return $this->prnt->body[und][0][value]=$str; 
   }
   
   
}

class LLBookInfo // works with book module 
{
  
 function __construct($node,$pid) // array of  progeny (generated) pages - nids
	{
         
		 return $node;
	}
	
   // @$plid - parent node 'mlid' - optinal parameter 
    static public  function TryOutline($node,$plid)// trying tp outline node
	{
	log_light("BOOKING IS HERE");
	  $result = 0;
	  if (empty($node->book['bid']))
	  {
	    $node->book['bid'] = variable_get('LL_book_id',727);
	     if (!empty($plid))  
        {		 
		  $node->book['plid']  = $plid;
		  log_light(" PLID =".$plid);
        } log_light("NO PLID");		
		$result = $node;
		 log_light("BOOKING IS WORK");
	  } else log_light("BOOK BID = ".$node->book['bid']);
	  
	  $node->book['has_children']=1; // it'll be useful in same case (if it really has children they'll be shown)
	  return $node;
	}
	
	// call next function it ypu know that
	static public  function GetPlid($node)
    {
	     $result = 0;
		 if (isset($node->book['mlid'])) 
		 {
		 $result=$node->book['mlid'];
		 log_light("book['mlid'] = ".$node->book['mlid']);
		 } else log_light("MLID IS UNKNOWN !");
		 return $result;
	}
}

/*класс описывает совокупность авто-списков страниц для каждой ноды*/
class  LlistSet { 

 // tags
	private  $opentag ;
	private  $closetag ;
	private  $cotag ; // current open tag
	private  $cctag ; // current close tag
	
	// lists set content
	private  $nltxt=""; // сюда записываем массив, фрагментов текста, выделенных тэгами LL модуля
	public   $lists ="" ; // массив списков  LL
	
	// list set info
	
	public  $prnt; // ссылка на родительский узел
	public $pid; //  id родителя
	public $mlid; //  id родителя = is parent $node->book['mlid']
	public  $body; // ссылка на текст тела родительского узла
	
	// funtions 
	
	function __construct($node)
	{
	   $this->opentag = variable_get('LL_open_tag','<ll>');
	   $this->closetag = variable_get('LL_close_tag','</ll>');
	   $this->cotag = $this->opentag;
	   $this->cctag = $this->closetag;
	  
	   $this->prnt = $node; 
	   $this->mlid = LLBookInfo::GetPlid($node);
	   
	   $this->pid = $node->nid; // BETA VERSION OF BACK LINK
       log_light("NID 000000= ".$node->nid);
 
	  $this->body =  $node->body['und']['0']['value'];
	   $str =  $node->body['und']['0']['value'];
	   $result = $this->GetData($str); // check here
	   
	 
	   
	   log_light("LlistSet __construct END || ");
	   
	   return $result; 
	}
	
	static public  function IsLlist($node)// quick check
	{
	  $result = 0;
	  $opentag = variable_get('LL_open_tag','<ll>');
	  $result = strs_btw_tags($opentag,$node->body['und']['0']['value'],1);
	  
	  return $result;
	}
	
	
	private function GetTxtFrags($opentag,$closetag,$str)
	{
		$this->nltxt = strs_btw_tags($opentag,$str,1); // get with tags

	   return $this->nltxt;
	}

	private function GetLists($str)
	{
	   $bltext = "[go back]";
	   log_light("--->GetTitles-- is started using << ".$str.">>");
	   if ($strarr = $this->GetTxtFrags($this->opentag,$this->closetag,$str)) // получаем список текстов списков
	   { 
			$lists= lists_from_strs ($strarr); // получаем список структурированных списков
			log_light("NID 11111111111= ".$this->prnt->nid);
			$result = $this->FillLists( $lists,$this->pid,$bltext);
			
			$result = 1;
			log_light("LISTSSSSSSS = ".arr_to_str($lists[0]));
	   }
	    else 
		{
			log_light("NO_MATCHES ++++++");
			$result = 0;
		}
	   
	   return $result;
	      
	} 
	
	 
	function GetData($str) // запускаем генерацию данных
	{
	   log_light("--Save-- is started !");
	   $result=""; // результат
	   if ($this->GetLists($str))   // ДОБАВИТЬ УСЛОВИЕ ПРОВЕРКИ!!! успешности извлечения информации относительно списка 
	   {
	      
	     $this->CreateNodes(); // запускаем создание страниц, имеющих отношение к спискам
			   
		  $result=1;
	   } else 
		   {
		   log_light("--- str_btw_tags()-- NOoOo matches ! ");
			 $result=0;
		   }	   
	   
	   return $result;  
	}

	private function CreateNodes()
	{
	  // log_light("NODE CREATION BLOCK");
	   $lists = $this->lists;
	   log_light("PRECREATION **+++====".arr_to_str($lists[0]->titles));
	   foreach ($lists as $list)
         {
		  log_light( arr_to_str($list->titles));
		   log_light("NODE CREATION BLOCK");
		   $list->Create();
		 }	   
	}
	
	private function FillLists($lists,$pid,$bltext)
	{
	   $arr[]=array();
	   $i=0;
	   foreach  ($lists as $titles)
	   {
	      $list =  new Llist($titles,$pid,$bltext,$this->mlid);
		  //log_light("FillLists = = =".arr_to_str($list->titles));
		  $arr[$i] = $list;
		 //log_light("FillLists 2= = =".arr_to_str($arr[$i]->titles));
		 $i++; 
 	   }
	//  log_light("PRECREATION22222 **+++====".arr_to_str($arr[0]));
	   $this->lists = $arr;
	    //log_light("PREPROMO = = =".arr_to_str($this->lists[0]->titles));
		print_r ($this->lists);
	   return $this->lists;
	}
	
	public function UpdateParentNodeBody()
	{
	   //log_light(">>>>>>>>>>>>>>>>>UpdateParentNodeBody()  IS STARTEEEEEEEEEEEED");
	   if ($arr = $this->nltxt)
	   {
		   $lists = $this->lists;
		   $i=0;
		   log_light("UpdateParentNodeBody = ".$arr);
		   foreach ($arr as $value)
		   {
			  
			  
			//  If (!$lists[$i]->IsLink($value)) // THIS NEED STANDARTIZATION
			//  {
			  $old="|".preg_quote($this->opentag.$value.$this->closetag)."|";// окружаем ограничителями - чтобы cтрока являлась регулярным выражением.
			  $new = $this->WrapInListTags($lists[$i]->PrintLinks());
			  $place = $this->body;
			
			  
			 // log_light(" THE ORIGINAL TEXT=". $place);
			 // log_light(" THE OLD TEXT=".$old);
			 // log_light(" THE NEW TEXT=".$new);
			  
			  
			 $rez= preg_replace( $old,$new,$place,1); // соответственно только одна замена для каждого списка
			 $this->body=$rez;
			  $i++;
			  log_light("  preg_replace() result =". $rez);
				log_light("  NEW BODY =".$this->body);
       //       }
		
					  
		   }
	   }
	  // log_light(">>>>>>>>>>>>>>>>>UpdateParentNodeBody()  IS ENDED");
	   return $this->body;
	}
	
	//private function ListAddTitles(list,$titles)
	private function Log($str) // ЭТУ ФУНКЦИЮ МОЖНО СДЕЛАТЬ ПУСТой
	{
	   
	   
	}
	
	public function WrapInListTags($str)
	{
	   return  $this->cotag.$str.$this->cctag ;
	   //return  "<ll>".$str."</ll>" ;
	   return  $str ;
	}

}

/* "фильтруем" LL - списки - делаем их "приятными" 
 для просмотра.*/
class  LLfilter // помимо создания класса "снаружи" следует вызвать метод обновления внешнего вида ноды
{
  
  private  $oldtxt=array(); // сюда записываем массив, фрагментов текста, выделенных тэгами LL модуля
  private  $newtxt=array(); // сюда записываем массив, фрагментов текста, выделенных тэгами LL модуля
  private  $body=array(); // исходный текст, который надо "отфильтровать"

  function __construct($str)
  {
		//log_light("=====================================<br>FILTER is started = ".$str);
		$result ="";
		
		$this->body=$str;
		  $opent= variable_get('LL_open_tag','<ll>');
	 // $closet = variable_get('LL_close_tag','</ll>');
	  $text = $str;
	  if ($this->oldtxt = $arr = strs_btw_tags($opent,$text,1))
	  {
		 log_light("this->oldtxt=".arr_to_str($arr));
		 //print_r($arr);
		  $newarr=array(); // временное хранилище для преобразованных нами фрагментов текста.
		 $i=0;
			foreach ($arr as $list)
			{
				 // log_light("<00000list======>".arr_to_str($arr)."</li>");		
				  $list = list_from_str($list);
                  if (count($list)>1) // if this's list (not line)
                  {				  
					  $newarr[$i]="<ul>"; // SHOULD BE REPLACED
					  foreach ($list as $val)
					  {
						$newarr[$i].=  "<li>".$val."</li>"; 
					   // log_light("<li==========>".$val."</li>");				
					   //$i++;
					  } 
					  $newarr[$i].="</ul>"; 
				} else $newarr[$i].=$list[0];
					$i++;
                 				
			 }
			 $this->newtxt=$newarr;
			 //log_light("this->newtxt=".arr_to_str( $newarr));
		  // $text =  "adfbbbbbbbb";
		
       //$result = $this->UpdateNodeView(); 	
	   }
	  // log_light("--- TXT RESULT = ".$text);
		//log_light("--- LL_filter_process IS ENDED ");
		//log_light("====================================== ");
	  return $result;
	  }
	  
	  
 
  
  public function UpdateNodeView()
  {
      log_light("------------------------------------ UPDATE IS STARTED (UpdateNodeView) ))) ");
      $rez="";
	  if (($txt = $this->body) && ($oldarr = $this->oldtxt) && ($newarr = $this->newtxt))
	  {
		// log_light("UpdateNodeView is started = ". $txt);
		 
		//  log_light("UpdateNodeView 1111) newarr= ".arr_to_str($newarr));
		//  log_light("UpdateNodeView 2222) oldarr= ".arr_to_str($oldarr));
		  
		 $i=0;
		 foreach ($oldarr as $val)
		  {
		   log_light("------------------------------------ UPDATE CONT(UpdateNodeView) ))) ");
		   $old="|".preg_quote($val)."|Us";
		   log_light("UpdateNodeView old = ". $old);
		   $new = $newarr[$i];
		   log_light("UpdateNodeView new =".$new );
		   $rez= preg_replace( $old,$new,$txt);
		   log_light("UpdateNodeView rez = ".$rez);
		   $txt = $rez;// remember result
		   $i++;
		  }
	 }
	  log_light("------------------------------------ UPDATE IS ENDED (UpdateNodeView) ))) ");
	  return $rez;
  }
 }